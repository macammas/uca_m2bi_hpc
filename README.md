#################################################################################
########              workflow for miRNA-seq data processing             ########
#################################################################################
#
# Workflow of miRNA-seq data processing
#
#
# Workflow made on stategra miRNA-seq data from :
# Gomez-Cabrero et al. - 2019 - STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse
# https://doi.org/10.1038/s41597-019-0202-7
# 

#### GET STARTED
#  
# clone gitlab project :

git clone https://gitlab.com/macammas/uca_m2bi_hpc.git

# and go inside the downloaded directory :

cd uca_m2bi_hpc

# The gitlab cloned project directory should contains this README
# and the following directories :
#	data/
#	└── miRNA/
#	    └── mmu-GRCm39-mature-miRNA.gff3 # miRNA positions on GRCm39
#
#	scripts/
#	├── stategra01_qc-init.slurm
#	├── stategra02_multiqc-init_by-exp.slurm
#	├── stategra03_trim.slurm
#	├── stategra04_qc-trim.slurm
#	├── stategra05_multiqc-trim_by-exp.slurm
#	├── stategra06_multiqc-trim_by-file.slurm
#	├── stategra07_mapping.slurm
#	├── stategra08_count.slurm
#	├── stategra09_count-norm.R
#	├── stategra09_count-norm.slurm
#	├── stategra10_count-final-results.slurm
#	├── stategra11_job-execution-info.slurm
#	├── stategra-efficiency_summary01_get_size.slurm
#	├── stategra-efficiency_summary02_plots.py
#	├── stategra-efficiency_summary02_plots.slurm
#	├── stategra-init_conda_python3.slurm
#	├── stategra-init_conda_R.slurm
#	├── stategra-init_R.R
#	├── stategra-init_SRA-download.slurm
#	├── stategra_miRNA-seq_workflow.sh
#	├── stategra_workflow_efficiency_test01_fastqc-init.slurm
#	├── stategra_workflow_efficiency_test02_trimming.slurm
#	├── stategra_workflow_efficiency_test03_mapping.slurm
#	├── stategra_workflow_efficiency_test04_count.slurm
#	└── stategra_workflow_efficiency_test05_count-norm.slurm
# 
# miRNA positions on GRCm38 where taken from miRBase (https://www.mirbase.org/)
# and converted into GRCm39 positions in Ensembl
# (https://www.ensembl.org/Tools/AssemblyConverter).
#
# scripts will be used to set up environnement, download raw datas,
# run miRNA-seq analysis, perform and sum up efficiency tests on some analysis.
#
#-------------------------------------------------------------------------------#


#### STEP 0
#
# Before doing anything intall the following modules :
#
#	conda/4.12.0
#	gcc/4.8.4
#	bedtools/2.27.1
#	gcc/8.1.0
#	cutadapt/1.18
#	python/3.7.1
#	MultiQC/1.7
#	HISAT2/2.0.5
#	java/oracle-1.11.0_11
#	picard/2.18.25
#	fastqc/0.11.7
#	sratoolkit/3.0.0
#
#-------------------------------------------------------------------------------#

#### STEP 1
#
# Hisat2 index of GRCm39 genome assembly have to be put in directory data.
# Resulting tree should look like that :
#	data/
#	└── hisat2/
#	    ├── Mus_musculus.1.ht2
#	    ├── Mus_musculus.2.ht2
#	    ├── Mus_musculus.3.ht2
#	    ├── Mus_musculus.4.ht2
#	    ├── Mus_musculus.5.ht2
#	    ├── Mus_musculus.6.ht2
#	    ├── Mus_musculus.7.ht2
#	    └── Mus_musculus.8.ht2
#
# For genome indexing, after genome downloading in :
#       data/
#       └── GRCm39/
#	    └── Mus_musculus.fasta
#	
# The following commands can be used :

mkdir -p data/hisat2
cd data
hisat2-build "GRCm39/Mus_musculus.fasta" "hisat2/Mus_musculus"

#
#-------------------------------------------------------------------------------#

#### STEP 1
#
# Initiate requiered conda environnement for R
# and install necessary packages.
#
# Once in project directory, run :

sbatch scripts/stategra-init_conda_R.slurm

# !!! Be aware that it could take some time and problems can appear when
# installing packages (often with BH package). If it appends go check in :
#	conda-env/R-count/lib/R/library/
# and remove packages mentionned in 00BLOCK directory and 00BLOCK directory
# itself.
# Then run again scripts/stategra-init_conda_R.slurm
#
# Packages NOISeq and sva should then be installed in R
#
# 
# Initiate requiered conda environnement for R
# and install necessary packages.
# run :

sbatch scripts/stategra-init_conda_python3.slurm

#
# Packages numpy, pandas and matplotlib then be installed in python3
#
# R and python3 conda environemant will be in directory :
#	conda-env/
#
# A log directory will be automatically created to contain slurm job log files.
#
#-------------------------------------------------------------------------------#

#### STEP 2
#
# Download Statgra miRNAseq raw datas.
# To download the 4 conditions at 0h and 24h, run :

sbatch scripts/stategra-init_SRA-download.slurm

# Fastq.gz files will be in :
#	data/
#	└── mirna-seq/
#	   ├── control_0h_B4_R1.fastq.gz
#	   ├── control_0h_B4_R2.fastq.gz
#	   ├── control_0h_B5_R1.fastq.gz
#	   ├── control_0h_B5_R2.fastq.gz
#	   ├── control_0h_B6_R1.fastq.gz
#	   ├── control_24h_B1_R1.fastq.gz
#	   ├── control_24h_B2_R1.fastq.gz
#	   ├── control_24h_B2_R2.fastq.gz
#	   ├── control_24h_B3_R1.fastq.gz
#	   ├── control_24h_B3_R2.fastq.gz
#	   ├── ikaros_0h_B4_R1.fastq.gz
#	   ├── ikaros_0h_B4_R2.fastq.gz
#	   ├── ikaros_0h_B5_R1.fastq.gz
#	   ├── ikaros_0h_B6_R1.fastq.gz
#	   ├── ikaros_24h_B1_R1.fastq.gz
#	   ├── ikaros_24h_B2_R1.fastq.gz
#	   └── ikaros_24h_B3_R1.fastq.gz
#
#-------------------------------------------------------------------------------#

#### STEP 3
#
# Run miRNAseq workflow analysis :
#

sbatch scripts/stategra_miRNA-seq_workflow.sh

# This scrip will perform read quality control, trimming, mapping, read counts
# on miRNA positions, counts normalization and a final summary on counts.
# Following main directories will be created :
#	results/
#	└── mirna-seq/
#	    ├── count/		# miRNA raw and normalized counts and summary.
#	    ├── mapping/	# bam files and index by condition.
#	    ├── qc/		# raw and trimmed read qc by condition.
#	    └── trim/		# fastq.gz trimmed reads.
#
	log/			# log files (already created at step 1 or 2)
#-------------------------------------------------------------------------------#

#### STEP 4 (optionnal, efficiency workflow)
#
# A workflow to test memory and cpu number parameters on computing efficiency.
# Tests done on raw quality control, trimming, mapping, miRNA read count and count normalization.
#
# Test run each analysis with 1, 2 and 3 cpu per task and with 500, 1000, 1500 and 2000 Mo for memory allowed per task.
#
# To perform those tests, run :

sbatch scripts/stategra_workflow_efficiency_test01_fastqc-init.slurm
sbatch scripts/stategra_workflow_efficiency_test02_trimming.slurm
sbatch scripts/stategra_workflow_efficiency_test03_mapping.slurm
sbatch scripts/stategra_workflow_efficiency_test04_count.slurm
sbatch scripts/stategra_workflow_efficiency_test05_count-norm.slurm

# Those scripts will remove and recreate datas in results/ directory.
# They will create the directory efficiency/ where execution time and parameters
# are recorded by task.
#
# !!! some results can be wrong, error may append when compution resources are
# low for some analyses.
#
# log files will be in log_efficiency directory.
#
#-------------------------------------------------------------------------------#

#### STEP 4 (optionnal, efficiency summary)
#
# Resume efficiency tests results in plots
#

sbatch scripts/stategra-efficiency_summary_plots.slurm

# Execution time plots for each tested parameters will be created by analysis
# in directory efficiency/
#
#-------------------------------------------------------------------------------#
