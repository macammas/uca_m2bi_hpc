#!/bin/bash
#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
##SBATCH --mem=3G
#SBATCH --mem-per-cpu=2G
#SBATCH --cpus-per-task=6
##SBATCH --array=0-16
#SBATCH --array=0-0
#SBATCH -o log/stategra-init_SRA-download-%A-%a
#SBATCH --job-name=stategra-init_SRA-download
#SBATCH --partition=short

# Get Stategra fastq.gz raw reads files from SRA archive
# (control and ikaros for 0h and 24h)
echo -e '\t\tDownload SRA accessions'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose
#set -euo pipefail

IFS=$'\n\t'

# get working directory path

WDIR=$PWD

# make log directory

mkdir -p $WDIR/log

# Upload packages

module purge
module load sratoolkit/3.0.0

# Set up directories

echo "Set up directories..." >&2

DATA_DIR=$WDIR/data
mkdir -p "$DATA_DIR"

# Set up arrays with SRA ids and output files names

tab_id=($(cat $WDIR/data/SRA_info_files_names.txt | sed 1d | cut -f2))
tab_names=($(cat $WDIR/data/SRA_info_files_names.txt | sed 1d | cut -f1))

# Set up output directories and output file

EXPERIMENT=mirna-seq

SCRATCHDIR=$WDIR/scratch/"$EXPERIMENT"/sra_download/${tab_names[$SLURM_ARRAY_TASK_ID]}
mkdir -p -m 700 "$SCRATCHDIR"

outfile="${tab_names[$SLURM_ARRAY_TASK_ID]}".fastq

OUTPUT="$DATA_DIR/$EXPERIMENT"

# Look for empty result directory
if [ -d "$OUTPUT" ]
then
	if [ "$(ls -A $OUTPUT/${outfile}.gz 2> /dev/null)" ]
	then
		echo "${tab_names[$SLURM_ARRAY_TASK_ID]} already exist in :"
		echo "$OUTPUT"
		#echo "it will be replaced."
		echo "no need to download again !"
		echo -e '\t\t---END---'
		exit
		#rm -f "$OUTPUT/${tab_names[$SLURM_ARRAY_TASK_ID]}"
	fi
else
	mkdir -p "$OUTPUT"
fi
echo "-----" >&2

# Run the program

echo "Start on $SLURMD_NODENAME: "`date` >&2
echo "In $OUTPUT :" >&2
echo "dowloading ${tab_id[$SLURM_ARRAY_TASK_ID]} as ${outfile}.gz ..." >&2

fasterq-dump --threads "$SLURM_CPUS_PER_TASK" \
	--bufsize 10B \
	--curcache 100M \
	--mem 1G \
	--details \
	--temp "$SCRATCHDIR" \
	--outfile "$outfile" \
	--outdir "$SCRATCHDIR" \
	${tab_id[$SLURM_ARRAY_TASK_ID]}

gzip "$SCRATCHDIR/$outfile"

# Mouve file from temporary directory into output directory

mv "$SCRATCHDIR"/*.gz "$OUTPUT"

# Cleaning in case something went wrong

rm -rf  "$SCRATCHDIR"

echo "Done !" >&2
echo -e '\t\t---END---'
