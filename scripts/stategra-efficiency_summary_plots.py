#!conda-env/python3/bin/python3

# Import libraries

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

######## Import tasks execution info

CWD = os.getcwd()
DATADIR = f"{CWD}/efficiency"

# Make a big dataframe (df) with all task info

df = pd.DataFrame()
for FileName in os.listdir(DATADIR) :
    if FileName.endswith(".out") :
        FilePath = f"{DATADIR}/{FileName}"
        df = pd.concat([df, pd.read_table(FilePath, header=0, sep="|")], axis=0)

#df.shape
#df.count()
#df.count


# Replace JobName by cleaned job name

job_names = df.JobName.unique()

clean_names = [name.replace("stategra_", "").replace("-", "_") for name in job_names]

replacement = dict(zip(job_names, clean_names))

df.replace(replacement, inplace=True)

# Transform values in numerical values

#df.ReqMem.unique()

replacement = dict(zip(df.ReqMem.unique(), [ram.replace("Mc", "") for ram in df.ReqMem.unique()]))

df.replace(replacement, inplace=True)

df["ReqMem"] = pd.to_numeric(df.ReqMem)
df["NCPUS"] = pd.to_numeric(df.NCPUS)
df["CPUTimeRAW"] = pd.to_numeric(df.CPUTimeRAW)

# Sort df by NCPUS and ReqMem

df.sort_values(["NCPUS", "ReqMem"], inplace=True)

#df.shape
#df.count()
#df.count

# Remove records with CPUTimeRAW < (mean CPUTimeRAW less 1.5 standard deviation)
# in each conditions (for some files scripts stoped earlyer with error) 

job_names = df.JobName.unique()

for job_name in job_names :
    sd = df[df.JobName==job_name].groupby(by=['NCPUS', 'ReqMem']).CPUTimeRAW.std()
    mean = df[df.JobName==job_name].groupby(by=['NCPUS', 'ReqMem']).CPUTimeRAW.mean()
    theshold = mean - (1.5 * sd)
    for multi_ind in theshold.index :
        NCPUS, ReqMem = multi_ind
        CPUTimeRAW = theshold[NCPUS][ReqMem]
        df.query('not (JobName==@job_name and NCPUS==@NCPUS and ReqMem==@ReqMem and CPUTimeRAW<@CPUTimeRAW)', inplace=True)

#df.shape
#df.count()
#df.count

# Compute mean CPUTimeRAW in all conditions for all steps
# Get mean CPUTime per CPU (CPUTimeRAW is cumulated CPU time execution)
# (Create a dictionnary of series.series for miRNA-seq process step)

d_exec_time = dict()
for job_name in job_names :
    d_exec_time[job_name] = df[df.JobName==job_name].groupby(by=['NCPUS', 'ReqMem']).CPUTimeRAW.mean()
    d_exec_time[job_name].name = job_name
    for multi_ind in d_exec_time[job_name].index :
        NCPUS, ReqMem = multi_ind
        d_exec_time[job_name][NCPUS][ReqMem] = d_exec_time[job_name][NCPUS][ReqMem] / NCPUS

# Plot execution timle vs NCPUS

#---------------
col=['', 'blue', 'orange', 'green', 'red']
for job_name in job_names :
    fig, ax = plt.subplots(1)
    nb_max_cpu = d_exec_time[job_name].index[-1][0]
    for i in range(1, nb_max_cpu + 1) :
        ax.plot(d_exec_time[job_name][i], color=col[i])
    xticks = d_exec_time[job_name][1].index # ticks on x axis
    ax.set_xticks(xticks)
    xaxis_labels = [str(tick) for tick in xticks] # ticks labels
    ax.set_xticklabels(xaxis_labels, rotation=45, ha='right')
    ax.set_xlabel("Memory per task (Mo)")
    ax.set_ylabel("Execution time (second)")
    ax.set_title(job_name) # plots title
    fig.legend([str(i) for i in range(1, nb_max_cpu + 1)],
            bbox_to_anchor=(1, 0.6), title='# CPU') # legend
    fig.set_size_inches([7, 5])
    outfile = f"{DATADIR}/{job_name}_plot_exec_times.pdf"
    fig.savefig(outfile, dpi=300, bbox_inches='tight')

