#!/bin/bash
#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
##SBATCH --mem=3G
#SBATCH --mem-per-cpu=500M
#SBATCH --cpus-per-task=1
##SBATCH --array=0-16
#SBATCH -o log/stategra_wf-%A
#SBATCH --job-name=stategra_wf
#SBATCH --partition=short

# Set working directory

WDIR=$PWD

# Make an array with all slurm scripts for miRNA-seq data precessing

tab=($(find $WDIR/scripts -type f -name "stategra[0-9][0-9]*.slurm" | sort))

## Run miRNA-seq data processing workflow

echo "Start on $SLURMD_NODENAME: "`date` >&2
echo "Run miRNA-seq data processing workflow..." >&2

# Initial QC
jid1=$(sbatch --parsable "${tab[0]}")
echo "$jid1 : Initial Quality Control" >&2

# Initial MultiQC - dependencies : Initial QC
jid2=$(sbatch --parsable --dependency=afterok:$jid1 "${tab[1]}")
echo "$jid2 : Initial MultiQC reports" >&2

# Trimming - dependencies : Initial QC, Initial MultiQC
jid3=$(sbatch --parsable "${tab[2]}")
echo "$jid3 : Trimming with Cutadapt tool" >&2

# Post QC - dependencies : Trimming
jid4=$(sbatch --parsable --dependency=afterok:$jid3 "${tab[3]}")
echo "$jid4 : Post Quality Control" >&2

# Post MultiQC by experiment - dependencies : Post QC
jid5=$(sbatch --parsable --dependency=afterok:$jid4 "${tab[4]}")
echo "$jid5 : Post MultiQC by experiment" >&2

# Post MultiQC by run - dependencies : Post QC
jid6=$(sbatch --parsable --dependency=afterok:$jid4 "${tab[5]}")
echo "$jid6 : Post MultiQC by run" >&2

# Mapping - dependencies : Trimming
jid7=$(sbatch --parsable --dependency=afterok:$jid3 "${tab[6]}")
echo "$jid7 : Mapping" >&2

# Raw count - dependencies : Mapping
jid8=$(sbatch --parsable --dependency=afterok:$jid7 "${tab[7]}")
echo "$jid8 : miRNA read count" >&2

# Count normalization (conda env., R and packages install) - dependencies : Raw count
jid9=$(sbatch --parsable --dependency=afterok:$jid8 "${tab[8]}")
echo "$jid9 : normalized miRNA read count" >&2

# Count final results - dependencies : Count normalization
jid10=$(sbatch --parsable --dependency=afterok:$jid9 "${tab[9]}")
echo "$jid10 : miRNA count final results" >&2
