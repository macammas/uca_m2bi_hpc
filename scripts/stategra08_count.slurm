#!/bin/bash
#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
##SBATCH --mem=3G
#SBATCH --mem-per-cpu=500M
#SBATCH --cpus-per-task=1
#SBATCH -o log/stategra_count-%A
#SBATCH --job-name=stategra_count
#SBATCH --partition=short

# Run miRNA read count
echo -e '\t\tQuantification'
echo "-------------------------------------"
echo "NCPU : $SLURM_CPUS_PER_TASK"
echo "MEM : $SLURM_MEM_PER_CPU"
echo "-------------------------------------"
echo

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose
#set -euo pipefail

IFS=$'\n\t'

# Get working directory path

WDIR=$PWD

# Upload whatever required packages

module purge
module load gcc/4.8.4 bedtools/2.27.1

# Set directories

echo "Set up directories..." >&2

EXPERIMENT=mirna-seq
DATA_DIR=$WDIR/results/"$EXPERIMENT"/mapping # mapped reads datas

# Get mapping bam files path
echo "get bam files, sorted by experiment..." >&2

tab=($(find "$DATA_DIR" -type f -name "*.bam" | sort))

echo "tab=" >&2
printf '%s\n' "${tab[@]/$DATA_DIR\/}" >&2
echo "-----------------" >&2

# Set output directories

SCRATCHDIR=$WDIR/scratch/"$EXPERIMENT"/count/count_raw
mkdir -p -m 700 "$SCRATCHDIR"

OUTPUT="$WDIR"/results/"$EXPERIMENT"/count/count_raw

outfile=mirna_count_raw.tab # output count file

# Look for empty result directory
if [ -d "$OUTPUT" ]
then
	if [ "$(ls -A $OUTPUT/$outfile)" ]
	then
		echo "$OUTPUT already exists and will be replaced."
		rm -rf "$OUTPUT"
	fi
fi
mkdir -p "$OUTPUT"



# Run the program

cd "$SCRATCHDIR"

echo "Start on $SLURMD_NODENAME: "`date` >&2
echo "Run quantification with bedtools multicov... " >&2

bed=$WDIR/data/miRNA/mmu-GRCm39-mature-miRNA.gff3 # miRNA positions

bedtools multicov -bams ${tab[@]} -bed "$bed" |
	cut -f9- |
	tr ";" "\t" |
	cut -f1,5- |
	sed 's/Name=//' |
	sort |
	uniq \
	> "$SCRATCHDIR/$outfile"

# Add field names to miRNA read count file

names=$(printf '%s\n' "${tab[@]/*\/}" | sed 's/_mapping\.bam//g')

header="miRNA\t""$(echo $names | tr ' ' '\t')"

sed -i 1i"$header" "$SCRATCHDIR/$outfile"

# Create experimental design file

exp_design="$SCRATCHDIR/exp_design_raw.tab"

paste <(echo "run" "$names" | tr " " "\n") \
	<(echo $names | tr " " "\n" |
	sed -e 's/.*control/C/' -e 's/.*ikaros/I/' -e 's/[hBR]//g' |
	awk 'BEGIN{FS="_"; OFS="\t";
		print "cond", "repB", "repT", "time", "batch"}
		{print $1, $3, $4, $2, $4}') \
	> "$exp_design"

# Mouve file from temporary directory to output directory

mv "$SCRATCHDIR"/*.tab "$OUTPUT"

# Cleaning in case something went wrong

rm -rf  "$SCRATCHDIR"
