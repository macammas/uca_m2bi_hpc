# Install R packages and dependecies for count normalisation : NOISeq and sva

if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager", repos="https://pbil.univ-lyon1.fr/CRAN/", quiet=TRUE)
dependencies="Imports"


# Packages NOISeq
if (!require("NOISeq", quietly = TRUE)){
	BiocManager::install("NOISeq", dependencies=TRUE, quiet=TRUE, update=FALSE)
	print("Intalling NOISeq and dependencies...")
	} else {
	print("Package NOISeq is already installed")
	}

# Packages sva

if (!require("sva", quietly = TRUE)){
	BiocManager::install("sva", dependencies=TRUE, quiet=TRUE, update=FALSE)
	print("Intalling sva and dependencies...")
	} else {
	print("packages sva is already installed")
	}
