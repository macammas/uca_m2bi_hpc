############# Snakemake stategra miRNA-seq data processing workflow #############
#### Run quality control on row reads (fastqc)
#### Run row reads trimming (cutadapt)
#### Run quality control on trimmed reads (fastqc)
#### Run reference genome indexing (hisat2)
#### Run trimmed reads mapping on reference genome (hisat2)
#### Run mapping files indexing (picard)
#### Run miRNA read count (bedtools)
#### Run miRNA read count normalization (R package NOISeq) !!! not working


# Import python modules

import os

# Set Directories
RAW_DATA="data/subset/" # raw fastq.gz files
GENOME="data/GRCm39/"	# reference genome fasta file
INDEX=GENOME + "hisat2/" # reference genome index
RAW_QC="results/qc_raw/"	# raw reads quality control reports
TRIM="results/trimming/"	# trimmed reads fastq.gz files
TRIM_QC="results/qc_trim/"	# trimmed reads quality control reports
MAP="results/mapping/"	# mapped read (sorted) bam files and indexes
COUNT="results/count/" # miRNA read counts matrix

# Get samples names (fastq.gz file basename)
SAMPLES=sorted([x.replace(".fastq.gz", "") for x in os.listdir(RAW_DATA)])

# Set genome basename and hisat2 indexes names
G_basename="Mus_musculus"
GENOME_INDEXES=[f"{G_basename}.{i}.ht2" for i in range(1,9)] # hisat2 indexes

# Set miRNA data base (from miRBase)
mirna_gff="data/miRNA/mmu-GRCm39-mature-miRNA.gff3" # gff mature miRNA file

# Set raw count matrix
raw_count_mat=COUNT + "raw_mirna_count.tab" # file name
header_count="mirna\t" + "\t".join(SAMPLES) # matrix header

# Set design matrix of experiment
raw_design_mat=COUNT + "exp_design_raw.tab" # file name
os.makedirs(COUNT, exist_ok=True) # make count results drectory
design_header=["run", "cond", "repB", "repT", "time", "batch"] # matrix header
with open(raw_design_mat ,"w") as f1: # create matrix
    f1.write("\t".join(design_header)+"\n")
    a=[[y[0], y[1][2], y[3][1], y[4][-1], y[2][0:-1], y[4][-1]] for y in  [[x] + x.split("_") for x in SAMPLES]]
    for i in a :
        f1.write("\t".join(i)+"\n")

# Set normalized count matrix
norm_count_mat=COUNT + "norm_mirna_count.tab" # file name
norm_design_mat=COUNT + "exp_design_norm.tab" # matrix design

#------

# Rules
rule all:
    input:
	# raw_fastqc
        expand(RAW_QC + "{sample}_fastqc.zip", sample=SAMPLES),
        expand(RAW_QC + "{sample}_fastqc.html", sample=SAMPLES),
        #
        # cutadapt
        expand(TRIM + "{sample}_trim.fastq.gz", sample=SAMPLES),
        #
        # trim_fastqc
        expand(TRIM_QC + "{sample}_trim_fastqc.zip", sample=SAMPLES),
        expand(TRIM_QC + "{sample}_trim_fastqc.html", sample=SAMPLES),
        # 
        # hisat2_genome_indexing
        expand(GENOME + "hisat2/{index}", index=GENOME_INDEXES),
        #
        # hisat2_mapping
        expand(MAP + "{sample}_trim.bam", sample=SAMPLES),
        #
        # picard_index
        expand(MAP + "{sample}_trim.bam.bai", sample=SAMPLES),
        #
        # multiBamcov
        raw_count_mat,
        #
        # noiseq
        norm_count_mat,
        norm_design_mat
        
rule raw_fastqc:
    input:
        expand(RAW_DATA + "{sample}.fastq.gz", sample=SAMPLES)

    output:
        expand(RAW_QC + "{sample}_fastqc.zip", sample=SAMPLES),
        expand(RAW_QC + "{sample}_fastqc.html", sample=SAMPLES)

    conda:
        "envs/qc.yml"

    threads: 4

    resources:
        mem_mb=500, 
        time="00:10:00"

    shell:
        "fastqc --outdir {RAW_QC} {input}"


rule cutadapt:
    input:
        RAW_DATA + "{sample}.fastq.gz"

    output:
        TRIM + "{sample}_trim.fastq.gz"

    conda:
        "envs/trim-map.yml"

    threads : 4

    resources:
        mem_mb=500, 
        time="00:10:00"

    shell:
        "cutadapt -j {threads} -a TGGAATTCTCGGGTGCCAAGG "
        "-e 0.25 -M 35 -m 15 -o {output} {input}"

rule trim_fastqc:
    input:
        expand(TRIM + "{sample}_trim.fastq.gz", sample=SAMPLES)

    output:
        expand(TRIM_QC + "{sample}_trim_fastqc.zip", sample=SAMPLES),
        expand(TRIM_QC + "{sample}_trim_fastqc.html", sample=SAMPLES)

    conda:
        "envs/qc.yml"

    resources:
        mem_mb=500, 
        time="00:10:00"

    shell:
        "fastqc --outdir {TRIM_QC} {input}"


rule hisat2_genome_indexing:
    input:
        GENOME + G_basename + ".fasta"

    output:
        expand(GENOME + "hisat2/{index}", index=GENOME_INDEXES)

    threads : 8
    
    conda:
        "envs/trim-map.yml"

    resources:
        mem_mb=500, 
        time="01:00:00"

    shell:
        "hisat2-build -p {threads} {input} {INDEX}{G_basename}"


rule hisat2_mapping:
    input:
        TRIM + "{sample}_trim.fastq.gz"

    output:
        MAP + "{sample}_trim.bam"

    threads: 4

    conda:
        "envs/trim-map.yml"

    resources:
        mem_mb=500, 
        time="00:15:00"

    shell:
        "hisat2 -p {threads} -x data/GRCm39/hisat2/Mus_musculus -U {input} | "
        "picard SortSam INPUT=/dev/stdin "
        "SORT_ORDER=coordinate "
        "OUTPUT=/dev/stdout | "
        "picard SamFormatConverter INPUT=/dev/stdin OUTPUT={output} "

rule picard_index:
    input:
        MAP + "{sample}_trim.bam"

    output:
        MAP + "{sample}_trim.bam.bai"

    conda:
        "envs/trim-map.yml"

    resources:
        mem_mb=500,
        time="00:10:00"

    shell:
        "picard BuildBamIndex INPUT={input} OUTPUT={output}"


rule multiBamcov:
    input:
        bam=expand(MAP + "{sample}_trim.bam", sample=SAMPLES),
        bed=mirna_gff

    output:
        raw_count_mat

    conda:
        "envs/read-count.yml"

    resources:
        mem_mb=500,
        time="00:10:00"

    shell:
        "echo {header_count} > {output}; "

        "bedtools multicov -bams {input.bam} -bed {input.bed} | " 
        "cut -f9- | tr ';' '\\t' | cut -f1,5- | "
        "sed 's/Name=//' | sort | uniq >> {output}"


rule noiseq:
    input:
        raw_count_mat,
        raw_design_mat

    output:
        norm_count_mat,
        norm_design_mat

    conda:
        "envs/read-count.yml"

    resources:
        mem_mb=500,
        time="00:10:00"

    script:
        "scripts/script_count-norm.R"
