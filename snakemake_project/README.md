############### miRNA-seq process workflow ######################

# workflow tested on stategra miRNA-seq data subset
# (Gomez-Cabrero et al. - 2019 - STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse)
# (https://doi.org/10.1038/s41597-019-0202-7)

# IMPORTANT !
# This workflow doesn't work well At least last rule "noiseq" (for read count normalization with a R script) doesn't works.
# It is recommended to run the workflow rules by rules by commentated others unprocessed rules input files in the "rule all" section => this workflow is not a workflow...

#### Step 0 : clone project, and work in the project directory
git clone https://gitlab.com/macammas/uca_m2bi_hpc.git

cd uca_m2bi_hpc/snakemake_project

# This directory should contain following directories :
nakemake_project
├── data
│   └── miRNA
│       └── mmu-GRCm39-mature-miRNA.gff3 # mature miRNA positions from miRBase
│                                        # (https://www.mirbase.org/) converted
│                                        # on GRCm39 with Ensemble tool
│                                        # (https://www.ensembl.org/Tools/AssemblyConverter)
├── README.md # Info for initialization to run the workflow
└── workflow
    ├── envs
    │   ├── qc.yml # special conda environnement for quality control (fastqc)
    │   ├── read-count.yml # special conda environnement for read count and
    │   │                  # normalization (bedtools, R)
    │   └── trim-map.yml # special conda environnement for trimming and mapping
    │                    # normalization (cutadapt, hisat2, picard)
    ├── scripts
    │   └── script_count-norm.R # R script for read count normalization 
    │                           # (packages NOISeq and sva)
    └── snakemake_mirna-seq_wf.smk # snakemake workflow

#### Step 1 : activate conda environnement with snakemake

#conda create -p ./conda-env/snakemake_project -c bioconda -c conda-forge gcc_linux-64=8.2.0 libgcc-ng=8.2.0 python=3.7.1 snakemake=7.15.1 # impossible, conflicts...

module load conda/4.12.0
conda activate
module load gcc/8.1.0  python/3.7.1 snakemake/7.15.1

#### Step 2 : create directory for reference genome, raw datas, and logs directory

mkdir -p data/GRCm39 # reference genome directory
mkdir -p logs # tasks logs execution directory

## Make symbolic links to datas or download datas
# Name fasta reference genome 'Mus_musculus.fasta' in 'data/GRCm39'
# -> ln -s /home/users/shared/data/Mus_musculus/current/ data/GRCm39/Mus_musculus.fasta
# Place raw miRNA-seq fastq file in directory 'data/subset'
# -> ln -s /home/users/shared/data/stategra/mirna-seq/ data/subset

#### Step 3 : run workflow workflow/stategra_mirna-seq_wf.smk

snakemake --cores 16 --jobs 100 --use-conda --snakefile workflow/snakemake_mirna-seq_wf.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=logs/slurm-%j.out" --latency-wait 60

# At the end, working directory should contain the following new directories :
snakemake_project
├── data
│   ├── GRCm39
│   │   ├── hisat2 # genome indexes
│   │   │   ├── Mus_musculus.1.ht2
│   │   │   ├── Mus_musculus.2.ht2
│   │   │   └── ...
│   │   └── Mus_musculus.fasta # reference genome
│   └── subset # raw data
│       ├── sscontrol_0h_B4_R1.fastq.gz
│       ├── sscontrol_0h_B4_R2.fastq.gz
│       └── ...
├── logs # slurm log
│   ├── slurm-56775.out
│   ├── slurm-56776.out
│   └── ...
└── results
    ├── count # raw miRNA read counts
    │   ├── exp_design_raw.tab
    │   └── raw_mirna_count.tab
    ├── mapping # reads alignement on genome
    │   ├── sscontrol_0h_B4_R1_trim.bam
    │   ├── sscontrol_0h_B4_R1_trim.bam.bai
    │   └── ...
    ├── qc_raw # raw read quality control
    │   ├── sscontrol_0h_B4_R1_fastqc.html
    │   ├── sscontrol_0h_B4_R1_fastqc.zip
    │   └── ...
    ├── qc_trim # trimmed read quality control
    │   ├── sscontrol_0h_B4_R1_trim_fastqc.html
    │   ├── sscontrol_0h_B4_R1_trim_fastqc.zip
    │   └── ...
    └── trimming # trimmed reads
        ├── sscontrol_0h_B4_R1_trim.fastq.gz
        ├── sscontrol_0h_B4_R2_trim.fastq.gz
        └── ...
